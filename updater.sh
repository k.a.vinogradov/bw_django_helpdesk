#!/bin/bash
git pull
python -m pip install -r requirements.txt
cd ./bioway_helpdesk
python ./manage.py collectstatic --noinput
python ./manage.py makemigrations
python ./manage.py migrate